defmodule OnefleetApi.Repo.Migrations.CreatePositions do
  use Ecto.Migration

  def change do
    create table(:positions) do
      add :created_at, :utc_datetime
      add :deleted_at, :utc_datetime
      add :created_by, references(:users, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:positions, [:created_by])
  end
end
