defmodule OnefleetApi.Repo.Migrations.AddGeomToPositions do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS postgis", ""

    alter table(:positions) do
      add :geom,  :geometry
    end
  end
end
