# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     OnefleetApi.Repo.insert!(%OnefleetApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# Add 50 random positions
for _ <- 1..50 do
  lat = Enum.random(-180..180)
  lon = Enum.random(-85..85)
  OnefleetApi.Repo.insert!(%OnefleetApi.Positions.Position{
    created_at: DateTime.truncate(DateTime.utc_now(), :second),
    geom: %Geo.Point{coordinates: {lat, lon}, srid: 4326}
  })
end
