defmodule OnefleetApi.PositionsTest do
  use OnefleetApi.DataCase

  alias OnefleetApi.Positions

  describe "positions" do
    alias OnefleetApi.Positions.Position

    import OnefleetApi.PositionsFixtures

    @invalid_attrs %{created_at: nil, deleted_at: nil, geom: 4}

    test "list_positions/0 returns all positions" do
      position = position_fixture()
      assert Positions.list_positions() == [position]
    end

    test "list_positions_fast/0 returns all positions (excluding timestamps)" do
      position = position_fixture()
      position = Map.put(position, :created_at, nil)
      position = Map.put(position, :updated_at, nil)
      position = Map.put(position, :deleted_at, nil)
      position = Map.put(position, :inserted_at, nil)
      assert Positions.list_positions_fast() == [position]
    end

    test "get_position!/1 returns the position with given id" do
      position = position_fixture()
      assert Positions.get_position!(position.id) == position
    end

    test "create_position/1 with valid data creates a position" do
      valid_attrs = %{
        created_at: ~U[2024-04-20 10:19:00Z],
        deleted_at: ~U[2024-04-20 10:19:00Z],
        geom: %Geo.Point{coordinates: {1.0, 1.0}, srid: 4326},
      }

      assert {:ok, %Position{} = position} = Positions.create_position(valid_attrs)
      assert position.created_at == ~U[2024-04-20 10:19:00Z]
      assert position.deleted_at == ~U[2024-04-20 10:19:00Z]
    end

    test "create_position/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Positions.create_position(@invalid_attrs)
    end

    test "update_position/2 with valid data updates the position" do
      position = position_fixture()
      update_attrs = %{created_at: ~U[2024-04-21 10:19:00Z], deleted_at: ~U[2024-04-21 10:19:00Z]}

      assert {:ok, %Position{} = position} = Positions.update_position(position, update_attrs)
      assert position.created_at == ~U[2024-04-21 10:19:00Z]
      assert position.deleted_at == ~U[2024-04-21 10:19:00Z]
    end

    test "update_position/2 with invalid data returns error changeset" do
      position = position_fixture()
      assert {:error, %Ecto.Changeset{}} = Positions.update_position(position, @invalid_attrs)
      assert position == Positions.get_position!(position.id)
    end

    test "delete_position/1 deletes the position" do
      position = position_fixture()
      assert {:ok, %Position{}} = Positions.delete_position(position)
      assert_raise Ecto.NoResultsError, fn -> Positions.get_position!(position.id) end
    end

    test "change_position/1 returns a position changeset" do
      position = position_fixture()
      assert %Ecto.Changeset{} = Positions.change_position(position)
    end
  end
end
