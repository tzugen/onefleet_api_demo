defmodule OnefleetApiWeb.PositionControllerTest do
  use OnefleetApiWeb.ConnCase

  import OnefleetApi.PositionsFixtures

  alias OnefleetApi.Positions.Position

  @create_attrs %{
    created_at: ~U[2024-04-20 10:19:00Z],
    deleted_at: ~U[2024-04-20 10:19:00Z],
    geom: %Geo.Point{coordinates: {1.0, 1.0}, srid: 4326}
  }
  @update_attrs %{
    created_at: ~U[2024-04-21 10:19:00Z],
    deleted_at: ~U[2024-04-21 10:19:00Z],
    geom: %Geo.Point{coordinates: {2.0, 2.0}, srid: 4326}
  }
  @invalid_attrs %{created_at: nil, deleted_at: nil, random: "blah", geom: 4}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all positions", %{conn: conn} do
      conn = get(conn, ~p"/api/v1/positions")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create position" do
    test "renders position when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/api/v1/positions", position: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/v1/positions/#{id}")

      assert %{
               "id" => ^id,
               "created_at" => "2024-04-20T10:19:00Z",
               "deleted_at" => "2024-04-20T10:19:00Z"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/api/v1/positions", position: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update position" do
    setup [:create_position]

    test "renders position when data is valid", %{conn: conn, position: %Position{id: id} = position} do
      conn = put(conn, ~p"/api/v1/positions/#{position}", position: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/v1/positions/#{id}")

      assert %{
               "id" => ^id,
               "created_at" => "2024-04-21T10:19:00Z",
               "deleted_at" => "2024-04-21T10:19:00Z"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, position: position} do
      conn = put(conn, ~p"/api/v1/positions/#{position}", position: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete position" do
    setup [:create_position]

    test "deletes chosen position", %{conn: conn, position: position} do
      conn = delete(conn, ~p"/api/v1/positions/#{position}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/v1/positions/#{position}")
      end
    end
  end

  defp create_position(_) do
    position = position_fixture()
    %{position: position}
  end
end
