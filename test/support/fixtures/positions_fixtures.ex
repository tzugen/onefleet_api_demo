defmodule OnefleetApi.PositionsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `OnefleetApi.Positions` context.
  """

  @doc """
  Generate a position.
  """
  def position_fixture(attrs \\ %{}) do
    {:ok, position} =
      attrs
      |> Enum.into(%{
        created_at: ~U[2024-04-20 10:19:00Z],
        deleted_at: ~U[2024-04-20 10:19:00Z],
        geom: %Geo.Point{coordinates: {1.0, 1.0}, srid: 4326},
      })
      |> OnefleetApi.Positions.create_position()

    position
  end
end
