## Just wrap the native erlang :json library with a syntax that phoenix expects
defmodule JSONWrapper do

  def decode!(json) do
    :json.decode(json)
  end

  def encode_to_iodata!(data) do
    :json.encode(data)
  end

  def encode!(data) do
    :json.encode(data)
  end

end
