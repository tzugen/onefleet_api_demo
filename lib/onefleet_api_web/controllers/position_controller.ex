defmodule OnefleetApiWeb.PositionController do
  use OnefleetApiWeb, :controller

  alias OnefleetApi.Positions
  alias OnefleetApi.Positions.Position

  action_fallback OnefleetApiWeb.FallbackController

  def index(conn, _params) do
    positions = Positions.list_positions_fast()
    render(conn, :index, positions: positions)
  end

  def index_since(conn, %{"timestamp" => timestamp}) do
    {:ok, time, _} = DateTime.from_iso8601(timestamp)
    positions =  Positions.list_positions_since(time)
    render(conn, :index, positions: positions)
  end

  def count(conn, _params) do
    count = Positions.count_positions()
    render(conn, :show, count: count)
  end

  def create(conn, %{"position" => position_params}) do
    with {:ok, %Position{} = position} <- Positions.create_position(position_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/v1/positions/#{position}")
      |> render(:show, position: position)
    end
  end

  def show(conn, %{"id" => id}) do
    position = Positions.get_position!(id)
    render(conn, :show, position: position)
  end

  def update(conn, %{"id" => id, "position" => position_params}) do
    position = Positions.get_position!(id)

    with {:ok, %Position{} = position} <- Positions.update_position(position, position_params) do
      render(conn, :show, position: position)
    end
  end

  def delete(conn, %{"id" => id}) do
    position = Positions.get_position!(id)

    with {:ok, %Position{}} <- Positions.delete_position(position) do
      send_resp(conn, :no_content, "")
    end
  end
end
