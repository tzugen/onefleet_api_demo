defmodule OnefleetApiWeb.PositionJSON do
  alias OnefleetApi.Positions.Position

  @doc """
  Renders a list of positions.
  """
  def index(%{positions: positions}) do
    %{data: for(position <- positions, do: data(position))}
  end

  @doc """
  Renders a single position.
  """
  def show(%{position: position}) do
    %{data: data_full(position)}
  end

  def show(%{count: count}) do
    %{count: count}
  end

  defp data(%Position{} = position) do
    %{
      id: position.id,
      geom: Tuple.to_list(position.geom.coordinates)
    }
  end

  defp data(%{"id" => id, "geom" => geom}) do
    %{
      id: id,
      geom: Tuple.to_list(geom.coordinates)
    }
  end

  defp data_full(%Position{} = position) do
    %{
      id: position.id,
      created_at: position.created_at,
      deleted_at: position.deleted_at,
      geom: Tuple.to_list(position.geom.coordinates)
    }
  end
end
