defmodule OnefleetApi.Positions do
  @moduledoc """
  The Positions context.
  """

  import Ecto.Query, warn: false
  alias OnefleetApi.Repo

  alias OnefleetApi.Positions.Position

  @doc """
  Returns the list of positions, without any timestamps loaded

  ## Examples

      iex> list_positions_fast()
      [%Position{
        id: 1,
        geom: %Geo.Point{coordinates: {1.0, 1.0}, srid: 4326},
        created_at: nil,
        updated_at: nil,
        deleted_at: nil,
        created_by: nil
      }, ...]

  """
  def list_positions_fast do
    Repo.all(from p in Position, select: [:id, :geom])
  end

  @doc """
  Returns the list of positions with all attributes loaded.

  ## Examples

      iex> list_positions()
      [%Position{}, ...]

  """
  def list_positions do
    Repo.all(Position)
  end



  @doc """
  Returns the list of which have been updated since the given timestamp.

  ## Examples

  iex> list_positions_since(DateTime.utc_now())
  [%Position{},...]

  """
  def list_positions_since(timestamp) do
    Repo.all(from p in Position, where: p.updated_at > ^timestamp, select: [:id, :geom])
  end

  @doc """
  Output the number of positions.

  ## Examples

  iex> count_positions()
  100

  """
  def count_positions do
    Repo.aggregate(Position, :count, :id)
  end

  @doc """
  Gets a single position.

  Raises `Ecto.NoResultsError` if the Position does not exist.

  ## Examples

      iex> get_position!(123)
      %Position{}

      iex> get_position!(456)
      ** (Ecto.NoResultsError)

  """
  def get_position!(id), do: Repo.get!(Position, id)

  @doc """
  Creates a position.

  ## Examples

      iex> create_position(%{field: value})
      {:ok, %Position{}}

      iex> create_position(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_position(attrs \\ %{}) do
    %Position{}
    |> Position.changeset(attrs)
    |> Repo.insert()
    |> broadcast_event("create", attrs)
  end

  @doc """
  Updates a position.

  ## Examples

      iex> update_position(position, %{field: new_value})
      {:ok, %Position{}}

      iex> update_position(position, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_position(%Position{} = position, attrs) do
    position
    |> Position.changeset(attrs)
    |> Repo.update()
    |> broadcast_event("update", position)
  end

  @doc """
  Deletes a position.

  ## Examples

      iex> delete_position(position)
      {:ok, %Position{}}

      iex> delete_position(position)
      {:error, %Ecto.Changeset{}}

  """
  def delete_position(%Position{} = position) do
    Repo.delete(position)
    |> broadcast_event("delete", position)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking position changes.

  ## Examples

      iex> change_position(position)
      %Ecto.Changeset{data: %Position{}}

  """
  def change_position(%Position{} = position, attrs \\ %{}) do
    Position.changeset(position, attrs)
  end

  def broadcast_event({:ok, position} , event, payload) do
    IO.inspect(event, label: "event")
    IO.inspect(payload, label: "payload")
    OnefleetApiWeb.Endpoint.broadcast!("position:changes", event, payload)
    {:ok, position}
  end

  def broadcast_event({:error, info}, _event, _payload) do
    {:error, info}
  end
end
