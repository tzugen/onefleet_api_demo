defmodule OnefleetApi.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      OnefleetApiWeb.Telemetry,
      OnefleetApi.Repo,
      {DNSCluster, query: Application.get_env(:onefleet_api, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: OnefleetApi.PubSub},
      # Start the Finch HTTP client for sending emails
      {Finch, name: OnefleetApi.Finch},
      # Start a worker by calling: OnefleetApi.Worker.start_link(arg)
      # {OnefleetApi.Worker, arg},
      # Start to serve requests, typically the last entry
      OnefleetApiWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: OnefleetApi.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    OnefleetApiWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
