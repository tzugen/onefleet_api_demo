defmodule OnefleetApi.Positions.Position do
  use Ecto.Schema
  import Ecto.Changeset

  schema "positions" do
    field :created_at, :utc_datetime
    field :deleted_at, :utc_datetime
    field :created_by, :id
    field :geom, Geo.PostGIS.Geometry

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(position, attrs) do
    position
    |> cast(attrs, [:created_at, :deleted_at, :geom])
    |> validate_required([:geom])
  end
end
