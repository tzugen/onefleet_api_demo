# Onefleet API

Sample API application.

- [x] Positions are stored as PostGIS geometries
- [x] API for **C**reating, **R**eading, **U**pdating, **D**eleting positions working with JSON replies
- [x] User managment
- [ ] Users can create and revoke API keys
- [x] WebSocket push notifications when new positions are published
- [x] Fast :rocket:

Available HTTP endpoints:
```
  GET     /api/v1/positions      
  GET     /api/v1/positions/:id  
  POST    /api/v1/positions      
  PATCH   /api/v1/positions/:id  
  PUT     /api/v1/positions/:id  
  DELETE  /api/v1/positions/:id  
```

There is a WebSocket available on /socket.
Any WebSocket library or client can subscribe to it and receive push message when new positions are published. API keys are also not checked here yet. 
There is a Javascript library to subscribe to channels: 
https://github.com/phoenixframework/phoenix/blob/main/assets/js/phoenix/channel.js

Alternatively any plain WebSocket library can be used.

## Running
1. Install Postgres and PostGIS, create a database and enable the PostGIS extension on it.
2. Configure database credentials in `config/dev.exs` or `config/prod.exs`
3. Run `mix setup` to install and setup dependencies
4. Start the API endpoint with `mix phx.server` 
  
Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


## Deploying
Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
